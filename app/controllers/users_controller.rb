class UsersController < ApplicationController
  def index
    @users = User.all 
  end

  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    
    if @user.save
      flash[:success] = "User successfully save."
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    
    if @user.update_attributes(user_params)
      flash[:success] = "User update successfully."
      redirect_to @user
    else
      render "edit"
      
    end
    
  end

  def show
    @user = User.find(params[:id])
  end

  def delete
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User destroy successfully."
    redirect_to users_url
  end
  
  private
  
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
